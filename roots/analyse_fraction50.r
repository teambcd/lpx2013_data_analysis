analyse_fraction50 <-function()
    {
    
    data_in=read.csv("Cdeciduous.csv")
    
    
    mat=as.numeric(data_in$mat[1:22])
    season=as.numeric(data_in$seasonality[1:22])
    MI=as.numeric(data_in$MI[1:22])
    PET=as.numeric(data_in$PET[1:22])
    MAP=as.numeric(data_in$MAP[1:22])
    root_frac=as.numeric(data_in$cal_fraction_at_50cm[1:22])
    
    mat[which(mat<0)]=NaN
    season[which(season<0)]=NaN
    MI[which(MI<0)]=NaN
    PET[which(PET<0)]=NaN
    MAP[which(MAP<0)]=NaN
    root_frac[which(root_frac<0)]=NaN

    
    plot(PET,MAP, col=cbind(root_frac,root_frac,root_frac)) 
 
    
    glm1=glm(root_frac~PET+MAP,family=gaussian(link="logit"))
    
    print(summary(glm1))
    
    np=length(root_frac)
    
    }