combine_relationships <-function() {
    source("find_LD_relationship.R")
    source("find_WD_relationship.R")
    source("find_CG_relationship.R")
    
    pdf('figures/lightning_analysis.pdf', height =9, width=4.5)
    par(mfrow=c(3,1),mar=c(5,5,1,1),oma=c(1,1,1,1))
    #layout(c(1,2,3))
    find_CG_relationship() 
    mtext('a) CG fraction',side = 3,line=-2,adj=0.99,cex=1)
    find_WD_relationship()
    mtext('b) Wet lightning',side = 3,line=-2,adj=0.99,cex=1)
    find_LD_relationship()
    mtext("c) 'Dry storm days'",side = 3,line=-3,adj=0.01,cex=1)
    
    
    dev.off()
}