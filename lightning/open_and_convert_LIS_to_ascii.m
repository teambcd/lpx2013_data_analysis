%Requires myIsField.m, available via google.

%Stuff to do next time:
% untar all the 2005 sciences files
% work out how to ref the first file in each day directory

close all
clear all
pathname='LIS_data/science/2005/2005/TRMM_LIS_SC.04.1_2005.001.hdf/';
day_fn_loc=[50 52];

range_lat=[25 50];
range_lon=[-130 -65];

% range_lat=[-90 90]
% range_lon=[-180 180]

ascdatai=zeros(10000000,8,'single');
p=0;

for d=1:365
    d
    if d<10
        pathname(day_fn_loc(1):day_fn_loc(1)+1)='00';
        pathname(day_fn_loc(2))=num2str(d);
    elseif d<100
        pathname(day_fn_loc(1))='0';
        pathname(day_fn_loc(2)-1:day_fn_loc(2))=num2str(d);
    else
        pathname(day_fn_loc(1):day_fn_loc(2))=num2str(d);
    end
    
    files=ls(pathname);
    
    [nfiles,~]=size(files);    
    
    for nf=3:nfiles
        
        filename=[pathname files(nf,:)]; 
        dtest=hdfinfo(filename);
        if myIsField(dtest,'HasPalette')
            data=hdfread(filename,'group');
            time93=data{1,1}';
            location=data{3,1}';
            lat=location(:,1);
            lon=location(:,2);

            [a,~]=size(time93);

            for f=1:a
                if lat(f)<range_lat(2) && lat(f)>range_lat(1) && ...
                        lon(f)<range_lon(2) && lon(f)>range_lon(1)            
                    p=p+1;
                    ascdatai(p,1)=lat(f);
                    ascdatai(p,2)=lon(f);

                    [year mon day hr min sec]=com_tai2utc(time93(f));        
                    ascdatai(p,3)=year;
                    ascdatai(p,4)=mon;
                    ascdatai(p,5)=day;
                    ascdatai(p,6)=hr;
                    ascdatai(p,7)=min;
                    ascdatai(p,8)=sec;
                end
            end
        end
    end
end

ascdata=ascdatai(1:p,:);
    
    