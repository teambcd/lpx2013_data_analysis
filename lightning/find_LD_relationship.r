find_LD_relationship <-function()
    {
    install_and_source_library("ncdf4")
    install_and_source_library("raster")
    install_and_source_library("Hmisc")
    source("libs/lat_size.r")
    #graphics.off()
    #CG file info
    CG_filename="NLDN_2005_lightning_CG_daily.nc"        
    CG_varname="CG" 
    
    #daily precip
    pr_filename="US_daily_precip/precip.V1.0.2005.nc"
    pr_filename="../../Inputs/Lightning_construction/US_daily_precip/precip.V1.0.2005.nc"
    pr_varname="precip" 

    #set up raster we want
    lat_range=c(25.25,49.75,0.5);#min lat, max lat, grid size
    lon_range=c(-129.75,-65.25,0.5); #ditto
    
    #Plot set up
    plot_name=" "
    xlabel="Monthly dry CG lightning strikes (strikes/km2/day)"
    ylabel="dry days with lightning (%)"
    ylimit=c(0,105)
    
    #Some parameters
     month_length=c(31,28,31,30,31,30,31,31,30,31,30,31)


    
#-------------------------------------------------------------------
    
    month_add=matrix(0,13)
    for (m in 1:12) {
        month_add[m+1]=month_add[m]+month_length[m]
    }
    
    # define rasters
    a=c((lon_range[2]-lon_range[1])/lon_range[3],(lat_range[2]-lat_range[1])/lat_range[3])
    
    req_raster=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=365)
    
    day_raster=raster(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2])
    
    req_raster[,]=0
    day_raster[,]=0
    
    
    #Setup latitue weighting.
    lat=day_raster
    lat[,]=1
    lat_range=c(ymin(lat),ymax(lat),res(lat)[1])
    vlat=seq(lat_range[2]-lat_range[3]/2,lat_range[1]+lat_range[3]/2,
        -lat_range[3])
    vlat=lat_size(vlat,lat_range[3])
    mlat=as.matrix(lat)
    mlat=sweep(mlat,MARGIN=1,vlat,'*')
    lat[,]=mlat
    
    
    #Open CG lightning
    CG=brick(CG_filename,CG_varname) 
    CG=resample(CG,req_raster)
    
    #Open precip
    
    pr=brick(pr_filename,pr_varname) 
    dextent=extent(xmin(pr)-360,xmax(pr)-360,ymin(pr),ymax(pr))
    extent(pr) <- dextent
    pr=resample(pr,req_raster)
    
    #arrange into monthly wd, dry lightn
    m=1
    md=0
    
    
    dry_lightn=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=12)
    dry_lightn[,]=0
    
    lightn_days=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=12)
    lightn_days[,]=0


    mdry_days=day_raster
    mlit_days=day_raster
    for (d in 1:365) {
        print(d)
        print(m)
        if (d>month_add[m+1]) {
           
            lightn_days[[m]]=mlit_days/mdry_days 
            m=m+1
            md=0  
            mdry_days=day_raster
            mlit_days=day_raster
        }
        md=md+1
        dry=pr[[d]]<0.0001
        strike=CG[[d]]>0
        strike=strike*dry
        
        mdry_days=mdry_days+dry
        mlit_days=mlit_days+strike
        
        CG_day=CG[[d]]        
        dry_lightn[[m]]=dry_lightn[[m]]+CG_day*dry/(month_length[m]*lat)
    }
    
    
    lightn_days[[m]]=mlit_days/mdry_days 
    
    #plot(dry_lightn[[1]])
    #dev.new(); plot(lightn_days[[1]])
    #make netcdf files
    
    #do comparisons
    dry_lightn=as.vector(values(dry_lightn))
    lightn_days=as.vector(values(lightn_days))
    
    lightn_days[which(lightn_days==1)]=NaN
    dry_lightn[which(dry_lightn==0)]=NaN
    test=is.na(lightn_days)+is.infinite(lightn_days)+
      is.na(dry_lightn)
      
    dry_lightn=dry_lightn[which(test==0)]
    lightn_days=lightn_days[which(test==0)]
    
    
    #dev.new()
    #plot(dry_lightn,lightn_days)
    #smoothScatter(dry_lightn,lightn_days,transformation = function(x) x^0.25)
    
    #bin
    
    nbins=100
    
    dbin=1*max(dry_lightn)/nbins
    
    bin_mean=matrix(NaN,nbins)
    bin_stdv=matrix(NaN,nbins)
    bin_nobs=matrix(NaN,nbins)
    
    for (bin in 1:nbins) {
      rbin=c(bin-1,bin)*dbin
      
      test=(dry_lightn>=rbin[1])+(dry_lightn<=rbin[2])
      test=lightn_days[which(test==2)]
      bin_mean[bin]=mean(test)
      bin_stdv[bin]=sd(test)
      bin_nobs[bin]=length(test)
    }
    print(sum(bin_nobs))
     #find best fit
    
    binx <- seq(0.005,0.995,length.out = 100)
    binx=binx*max(dry_lightn)
    rm(p1,p2)
    #fit=nls(lightn_days~(1-1/((dry_lightn+1)**p2)),
    #  algorithm = "port")
    
    fit=nls(lightn_days~(1-1/(p1*((dry_lightn+1)**p2))),
      algorithm = "port",start = list(p1 = 1.1, p2 = 92338))


    summ=print(summary(fit))
    p1=summ$coefficients[1] 
    p2=summ$coefficients[2]   
    print(p1)
    print(p2)
    
    bin_plus=bin_mean+bin_stdv    
    bin_plus[which(bin_plus>1)]=1
    
    bin_minus=bin_mean-bin_stdv    
    bin_minus[which(bin_minus<0)]=0
    
    x=binx# <- seq(0,1,length.out = 10000)*max(dry_lightn)

    y <- 1-1/(p1*((x+1)**p2))    

    yold=x/x
    
    y=y*100
    yold=yold*100
    bin_mean=bin_mean*100
    bin_plus=bin_plus*100
    bin_minus=bin_minus*100
    
    #lines(x,y,col='red')
    #dev.new()
    binx=binx*1000000
    x=x*1000000
    #plot(bin_mean,binx,,xaxs='i',yaxs='i',
    #    main=plot_name,log='xy',
    #    xlab=xlabel,
    #    ylab=ylabel,type = "n", cex=.5,xlog=TRUE)
        
    errbar(binx,bin_mean,bin_plus,bin_minus,
        main=plot_name,
        xlab=xlabel,
        ylab=ylabel,
        ylim=ylimit,
        xaxs='i')
    
    lines(x,y,col='red')
    print(mean(lightn_days))
    lines(c(0,100),c(mean(lightn_days),mean(lightn_days))*100,col='red',lty=2)
    lines(x,yold,col='blue')
    
    #dev.new()
    #plot((1:100)-0.05,bin_nobs)
    #then copy comparions from CG file
    
    
    
    
    #do comparisons
  }