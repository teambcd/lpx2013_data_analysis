open_as_arrange <-function()
  {
  
    library("raster")
    library("ncdf")
    library("RNetCDF")
    source("libs/com_utc2tai.r")
  # Opens up raw NLDN lightning data, and regrids to a standard LPX
  # half degree grid
  # Raw data is ciolumn ascii of date, time, lat, lon, current, CG 
  # or IC strike, and number of strikes in following format:
  # yyyy-mm-dd hh:mm:ss.ss la.titu lo.ngdi +cc.c G/C n
  
  # first order output is to grid number of cloug or ground strikes
  # on a half degree grid.
  
    #Some intial info
    #file information (name and length
    filename="2005_NLDN_dT4KGM_truncated.txt"
    nline=33133994
    
    #grid info, 
    lon_range=c(-130,-60)
    lat_range=c(20,55)
    dcell=0.5
    
    #system info
    line_per_loop=100000
    
    #Outputs info
    filename_out=c("NDLN_days/NDLN_ddd.csv") #CV
    string_day_loc=c(16,18)

    #Some parameters
    month_length=c(31,28,31,30,31,30,31,31,30,31,30,31)
    

    
#-------------------------------------------------------------------
    
    month_add=matrix(0,12)
    for (m in 1:11) {
        month_add[m+1]=month_add[m]+month_length[m]
    }


    #open file x lines at a time and use rasterize
    
    ptm <- proc.time()
    nloops=ceiling(nline/line_per_loop)

    
    for (i in 1:nloops) {
        
        
        if (i==nloops) {
            nlines=(nline-(i-1)*line_per_loop)
        } else {
            nline=line_per_loop
        }
        
        
        # Opens this set of data in one long vector of strings
        fline=scan(file=filename,what=character(),
            nmax=7*line_per_loop,skip=(i-1)*line_per_loop)
        
        # arranges in table
        fline=array(fline,c(7,line_per_loop))

        #seperate out information
        years=as.numeric(substring(fline[1,],1,4))
        month=as.numeric(substring(fline[1,],6,7))
        day=as.numeric(substring(fline[1,],9,10))
        yday=month_add[month]+day
        print(max(yday))
        hour=as.numeric(substring(fline[2,],1,2))
        mins=as.numeric(substring(fline[2,],4,5))
        sec=as.numeric(substring(fline[2,],7,12))
        
        lat=as.numeric(fline[3,])
        lon=as.numeric(fline[4,])
        type=fline[6,] 
        
        tai93=com_utc2tai(years,month,day,hour,mins,sec)

        rm(fline)
        
        tai93=tai93[which(type=="G")]
        lat=lat[which(type=="G")]
        lon=lon[which(type=="G")]
        yday=yday[which(type=="G")]
                
        rm(type,years,month,day,hour,mins,sec)
        
        #------------------------------------------------------------
        #find which file which row shoud go in 
        
        ncg=length(yday)
        ydayi=c(yday[1],yday[1:(ncg-1)])
        
        file_swop=c(1,which(yday!=ydayi),ncg+1)

        nf=length(file_swop)
        
        for ( f in 2:nf) {
            sf=file_swop[f-1]
            ef=file_swop[f]-1
            if (yday[sf]<141) {
                next
            }
            if (yday[sf]<10) {
                substring(filename_out,string_day_loc[1],
                    string_day_loc[1]+1)='00'
                substring(filename_out,string_day_loc[2])=
                as.character(yday[sf])
            } else if(yday[sf]<100) {
                substring(filename_out,string_day_loc[1])='0'
                substring(filename_out,string_day_loc[1]+1,
                    string_day_loc[2])=as.character(yday[sf])
            } else {
                substring(filename_out,string_day_loc[1],
                    string_day_loc[2])=as.character(yday[sf])
            }
               # print(lat) 
               # print(sf:ef)
            out=cbind(lat[sf:ef],lon[sf:ef],tai93[sf:ef],yday[sf:ef])
            
            
            
            write.table(out,filename_out,append=TRUE,sep=",",
                row.names = FALSE,col.names = FALSE)
            # if ( file.exists(filename_out)) {
                # write.csv(out,filename_out,append=TRUE)
            # } else {
                # write.csv(out,filename_out)
            # }
            
        }
            
        print(paste(round(100*i/nloops,digits = 4),"% through dataset analysis"))
    }
        
        
 
    time_taken=proc.time() - ptm
      
    
    return(time_taken)
  }
        
    
    