%Requires myIsField.m, available via google.

%Stuff to do next time:
% untar all the 2005 sciences files
% work out how to ref the first file in each day directory

close all
clear all
LIS_pathname='LIS_data/science/2005/2005/TRMM_LIS_SC.04.1_2005.001.hdf/';
day_LIS_pn_loc=[50 52];

NDLN_file='NDLN_days/NDLN_001.csv';
day_NDLN_fn_loc=[16 18];


range_lat=[25.25 49.75 0.5]; %min lat, max lat, grid size
range_lon=[-129.75 -65.25 0.5]; %ditto

ml=[31 28 31 30 31 30 31 31 30 31 30 31];

for i=2:12
    ml(i)=ml(i)+ml(i-1);
end

% range_lat=[-90 90]
% range_lon=[-180 180]

a=1+(range_lon(2)-range_lon(1))/range_lon(3);
b=1+(range_lat(2)-range_lat(1))/range_lat(3);

p=0;
m=1;
count=zeros(a,b,12,5,'single');
    %lon by lat by month by field., Fields:
    %1. no. LIS observations
    %2. no. ground observations during LIS coverage
    %3. total no. ground observations for the month
    %4. starts off as effective obs time. Turns into fraction CG
    %5. starts off as max obs time. Turns into scaled tot lightning
    %6. starts off nithung. Turns into observation time
for d=1:365
    d  
    
    if d>ml(m)
        m=m+1;
    end
    
    if d<10
        LIS_pathname(day_LIS_pn_loc(1):day_LIS_pn_loc(1)+1)='00';
        LIS_pathname(day_LIS_pn_loc(2))=num2str(d);
        
        NDLN_file(day_NDLN_fn_loc(1):day_NDLN_fn_loc(1)+1)='00';
        NDLN_file(day_NDLN_fn_loc(2))=num2str(d);
    elseif d<100
        LIS_pathname(day_LIS_pn_loc(1))='0';
        LIS_pathname(day_LIS_pn_loc(2)-1:day_LIS_pn_loc(2))=num2str(d);
        
        NDLN_file(day_NDLN_fn_loc(1))='0';
        NDLN_file(day_NDLN_fn_loc(2)-1:day_NDLN_fn_loc(2))=num2str(d);
    else
        LIS_pathname(day_LIS_pn_loc(1):day_LIS_pn_loc(2))=num2str(d);
        
        NDLN_file(day_NDLN_fn_loc(1):day_NDLN_fn_loc(2))=num2str(d);
    end
    
    files=ls(LIS_pathname);
    
    [nfiles,~]=size(files);    
    
    NDLN=csvread(NDLN_file);
    
    %% Ground observations will go here
    NDLN_lat=NDLN(:,1);
    NDLN_lon=NDLN(:,2);
    NDLN_tai93=NDLN(:,3);

    ii_NDLN=round((NDLN_lon-range_lon(1))/range_lon(3)+1);
    jj_NDLN=round((NDLN_lat-range_lat(1))/range_lat(3)+1);

    
    ii0=ii_NDLN;    
    ii_NDLN=ii_NDLN(find(ii0>0));
    jj_NDLN=jj_NDLN(find(ii0>0));
    

    
    ii0=ii_NDLN;    
    ii_NDLN=ii_NDLN(find(ii0<a));
    jj_NDLN=jj_NDLN(find(ii0<a));
    


    jj0=jj_NDLN;    
    ii_NDLN=ii_NDLN(find(jj0>0));
    jj_NDLN=jj_NDLN(find(jj0>0));
    

    
    jj0=jj_NDLN;    
    ii_NDLN=ii_NDLN(find(jj0<b));
    jj_NDLN=jj_NDLN(find(jj0<b));
    
    [nflash,~]=size(ii_NDLN);
    
    for flash=1:nflash
        count(ii_NDLN(flash),jj_NDLN(flash),m,3)= ...
            count(ii_NDLN(flash),jj_NDLN(flash),m,3)+1;
    end
    
    for nf=3:nfiles   
        filename=[LIS_pathname files(nf,:)]; 
        dtest=hdfinfo(filename);

        data=hdfread(filename,'viewtime');

        location=data{1,1}';
        lat=location(:,1);
        lon=location(:,2);
        
        ii=round((lon-range_lon(1))/range_lon(3)+1);
        jj=round((lat-range_lat(1))/range_lat(3)+1);

        stai93=data{2,1}';
        etai93=data{3,1}';
        dtai93=data{4,1}';
        
        [nt,~]=size(stai93);
        
        test=(ii>0) + (ii<a) + (jj>0) + (jj<b);
        
        rpoint=find(test==4);
        
        ii=ii(rpoint);
        jj=jj(rpoint);
        stai93=stai93(rpoint);
        etai93=etai93(rpoint);
        dtai93=dtai93(rpoint);
        
        [npoint,~]=size(rpoint);
        
        for pnt=1:npoint
            count(ii(pnt),jj(pnt),m,4)=count(ii(pnt),jj(pnt),m,4)+...
                double(etai93(pnt)-stai93(pnt));
            count(ii(pnt),jj(pnt),m,5)=count(ii(pnt),jj(pnt),m,5)+...
                double(dtai93(pnt));
        end
        
        %% ground obervsations that strike during sat overhead
        ii0=ii;
        jj0=jj;
        for flash=1:nflash
            ii=ii0;
            jj=jj0;
                   
            
            test=(ii_NDLN(flash)==ii)+(jj_NDLN(flash)==jj)+...
                (NDLN_tai93(flash)>stai93)+(NDLN_tai93(flash)<etai93);
            
            testi=[(ii_NDLN(flash)==ii) (jj_NDLN(flash)==jj) ...
                (NDLN_tai93(flash)>stai93) (NDLN_tai93(flash)<etai93)];
            
            test2=testi(:,1)+testi(:,2);
            
            ii=ii(find(test==4));
            jj=jj(find(test==4));
            
            [ncommon,~]=size(ii);
            
            
            for nc=1:ncommon
                count(ii_NDLN(flash),jj_NDLN(flash),m,2)=...
                    count(ii_NDLN(flash),jj_NDLN(flash),m,2)+1;
            end
        end
            
        
        
        %% LIS observations
        if myIsField(dtest,'HasPalette')
            data=hdfread(filename,'group');
            location=data{3,1}';
            LIS_lat=location(:,1);
            LIS_lon=location(:,2);
            
            ii_LIS=round((LIS_lon-range_lon(1))/range_lon(3)+1);
            jj_LIS=round((LIS_lat-range_lat(1))/range_lat(3)+1);

            [nLIS,~]=size(LIS_lat);            

            for f=1:nLIS 
                if ii_LIS(f)>0 && jj_LIS(f)>0 && ii_LIS(f)<a && jj_LIS(f)<b  
                    count(ii_LIS(f),jj_LIS(f),m,1)= ...
                        count(ii_LIS(f),jj_LIS(f),m,1)+1;
                    
                end
            end
        end
        
                
                
    end
end

save nn
counti=count;
    count(:,:,:,1)=count(:,:,:,1).*count(:,:,:,4)./count(:,:,:,5);
    
    count(:,:,:,6)=count(:,:,:,4);
    count(:,:,:,4)=count(:,:,:,2)./count(:,:,:,1);
    count(:,:,:,5)=count(:,:,:,1)./count(:,:,:,4);
    

temp=count(:,:,:,1);
temp(find(temp(:,:,:,1)==0))=nan;
temp(find(count(:,:,:,2)==0))=nan;
count(:,:,:,1)=temp;


for i=4:6
    temp=count(:,:,:,i);
    temp(find(isnan(count(:,:,:,1))))=nan;
    count(:,:,:,i)=temp;
end

masked=count;
test=zeros(a,b);
load mask2
for i=1:a
    lon=range_lon(1)+(i-1)*range_lon(3);    
    jj=1+(lon+179.75)*2;
    for j=1:b
        lat=range_lat(1)+(j-1)*range_lat(3);
        ii=1+(lat+89.75)*2;
%         ii=360-ii+1;
        
        if landmask_0k(ii,jj)==-999
            masked(i,j,:)=nan;
            test(i,j)=1;
        end
    end
end
        




%% write netcdf files
filename='CG_LIS_NDLN4.nc';
varnames={'LIS','NLDN','NLDN_total','CG','lightn_total','obs_time'};
longname={'LIS flash count', 'NLDN CG lightning during overpass',...
    'NLSN CG lightning','fraction CG lightning','scaled total lightning',...
    ' observation time'};
    
description={'LIS lightning flash count',...
             'NLDN flashes recorded whilst LIS instrument was overhead',...
             'NLDN total recorded flashes',...
             'percentage of CG flashes, equal to NLDN/LIS',...
             'estimate of total lightning obtained NLDN_total/CG',...
             'effective observation time by satalite'};
         
source={'LIS HDF files, obtained at http://gcmd.nasa.gov/records/GCMD_lislip.html',...
        'NLDN text file, purchased from vaisala',...
        'NLDN text file, purchased from vaisala',...
        'see LIS and NLDN variables',...
        'NLDN (see NLSN variable) scaled by proportion CG',...
        'sum of LIS HDF (see var1) effective observation time for each .5 degree cover'};
    
    
nccreate(filename,'lon','Dimensions',{'lon' a})
ncwrite(filename,'lon',range_lon(1):range_lon(3):range_lon(2))

nccreate(filename,'lat','Dimensions',{'lat' b})
ncwrite(filename,'lat',range_lat(1):range_lat(3):range_lat(2))

nccreate(filename,'time','Dimensions',{'month' 12})
ncwrite(filename,'time',1:12)

for i=1:6
    nccreate(filename,varnames{1,i},'Dimensions',....
        {'lon' a 'lat' b 'month'},'FillValue',-999)
    ncwrite(filename,varnames{1,i}, count(:,:,:,i));
end
    
%% write masked file
filename='CG_LIS_NDLN4_masked.nc';
    
    
nccreate(filename,'lon','Dimensions',{'lon' a})
ncwrite(filename,'lon',range_lon(1):range_lon(3):range_lon(2))

nccreate(filename,'lat','Dimensions',{'lat' b})
ncwrite(filename,'lat',range_lat(1):range_lat(3):range_lat(2))

nccreate(filename,'time','Dimensions',{'month' 12})
ncwrite(filename,'time',1:12)

for i=1:6
    nccreate(filename,varnames{1,i},'Dimensions',....
        {'lon' a 'lat' b 'month'},'FillValue',-999)
    ncwrite(filename,varnames{1,i}, masked(:,:,:,i));
end
    
 

    
    