#-----------------------------------------------------------------------
# UTC to TAI93
#-----------------------------------------------------------------------
com_utc2tai <- function(iy,im,id,ih,imin,sec)
  {


  mins = 60.0
  hour = 60.0*mins
  day = 24.0*hour
  year = 365.0*day
  mdays = c(31,28,31,30,31,30,31,31,30,31,30,31)

  tai93 = (iy-1993)*year + floor((iy-1993)/4.0)*day
  days = id -1
  
   
  for ( i in 1:12) {
    days[which(im>i)]=days[which(im>i)]+mdays[i]
    # if(im > i) {
        # days = days + mdays(i)
    # }
  }
  
  days[which(((iy %% 4) ==0 + (im>2))==2)]=
    days[which(((iy %% 4) ==0 + (im>2))==2)]+1  
  # if((iy %% 4) == 0 .AND. im > 2) {
    # days = days + 1 !leap year
  # }
  
  tai93 = tai93 + (days)*day + (ih)*hour + (imin)*mins + sec
  
  tai93[which(((iy>1993)+(((iy==1993)+(im>6))==2))>0)]=
    tai93[which(((iy>1993)+(((iy==1993)+(im>6))==2))>0)]+1
  # if(iy > 1993 || (iy==1993 && im > 6)) {
    # tai93 = tai93 + 1.0 #leap second
  # }
  
  tai93[which(((iy>1994)+(((iy==1994)+(im>6))==2))>0)]=
    tai93[which(((iy>1993)+(((iy==1993)+(im>6))==2))>0)]+1
  # IF(iy > 1994 || (iy==1994 && im > 6)) tai93 = tai93 + 1.0d0 !leap second
  
  tai93[which(iy>1995)]=tai93[which(iy>1995)]+1
  # IF(iy > 1995) tai93 = tai93 + 1.0d0 !leap second
  
  tai93[which(((iy>1997)+(((iy==1997)+(im>6))==2))>0)]=
    tai93[which(((iy>1997)+(((iy==1997)+(im>6))==2))>0)]+1
  # IF(iy > 1997 .OR. (iy==1997 .AND. im > 6)) tai93 = tai93 + 1.0d0 !leap second
  
  tai93[which(iy>1998)]=tai93[which(iy>1998)]+1
  # IF(iy > 1998) tai93 = tai93 + 1.0d0 !leap second
  
  tai93[which(iy>2005)]=tai93[which(iy>2005)]+1
  # IF(iy > 2005) tai93 = tai93 + 1.0d0 !leap second
  
  tai93[which(iy>2008)]=tai93[which(iy>2008)]+1
  # IF(iy > 2008) tai93 = tai93 + 1.0d0 !leap second
  
  return(tai93)
} # com_utc2tai

# !-----------------------------------------------------------------------
# ! TAI93 to UTC
# !-----------------------------------------------------------------------
# SUBROUTINE com_tai2utc(tai93,iy,im,id,ih,imin,sec)
  # IMPLICIT NONE
  # INTEGER,PARAMETER :: n=7 ! number of leap seconds after Jan. 1, 1993
  # INTEGER,PARAMETER :: leapsec(n) = (/  15638399,  47174400,  94608001,&
                                  # &    141868802, 189302403, 410227204,&
                                  # &    504921605/)
  # REAL(r_size),INTENT(IN) :: tai93
  # INTEGER,INTENT(OUT) :: iy,im,id,ih,imin
  # REAL(r_size),INTENT(OUT) :: sec
  # REAL(r_size),PARAMETER :: mins = 60.0d0
  # REAL(r_size),PARAMETER :: hour = 60.0d0*mins
  # REAL(r_size),PARAMETER :: day = 24.0d0*hour
  # REAL(r_size),PARAMETER :: year = 365.0d0*day
  # INTEGER,PARAMETER :: mdays(12) = (/31,28,31,30,31,30,31,31,30,31,30,31/)
  # REAL(r_size) :: wk,tai
  # INTEGER :: days,i,leap

  # tai = tai93
  # sec = 0.0d0
  # DO i=1,n
    # IF(FLOOR(tai93) == leapsec(i)+1) sec = 60.0d0 + tai93-FLOOR(tai93,r_size)
    # IF(FLOOR(tai93) > leapsec(i)) tai = tai -1.0d0
  # END DO
  # iy = 1993 + FLOOR(tai /year)
  # wk = tai - REAL(iy-1993,r_size)*year - FLOOR(REAL(iy-1993)/4.0,r_size)*day
  # IF(wk < 0.0d0) THEN
    # iy = iy -1
    # wk = tai - REAL(iy-1993,r_size)*year - FLOOR(REAL(iy-1993)/4.0,r_size)*day
  # END IF
  # days = FLOOR(wk/day)
  # wk = wk - REAL(days,r_size)*day
  # im = 1
  # DO i=1,12
    # leap = 0
    # IF(im == 2 .AND. MOD(iy,4)==0) leap=1
    # IF(im == i .AND. days >= mdays(i)+leap) THEN
      # im = im + 1
      # days = days - mdays(i)-leap
    # END IF
  # END DO
  # id = days +1

  # ih = FLOOR(wk/hour)
  # wk = wk - REAL(ih,r_size)*hour
  # imin = FLOOR(wk/mins)
  # IF(sec < 60.0d0) sec = wk - REAL(imin,r_size)*mins

  # RETURN
# END SUBROUTINE com_tai2utc