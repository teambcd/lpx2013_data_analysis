simple_add_or_find_in_lookup_table <- function(attribute,
        filename_lookup,rename_attribute=FALSE,
        return_new_table=FALSE) {
    ##############################################################
    ## function set-up                                          ##
    ##############################################################    
    ## load lookup table                                        ##
    lookup <<- read.table(filename_lookup,
        sep=",",header=TRUE,stringsAsFactors = FALSE,
        row.names=1)
    
    ## find choice when adding a new attribute row
    choices=colnames(lookup)
    
    ## set up cat for asking what a new attribute can take
    nchoices=0
    choices_display=""
    for (i in choices) {
        nchoices=nchoices+1
        choices_display=paste(choices_display,nchoices,".","\t",i,"\n",sep="")
    }
    
    ##############################################################
    ## Classify things that are already in teh lookup table     ##
    ##############################################################    
    att_tabled=rownames(lookup)
    l=dim(lookup)
    s=length(attribute)
    
    classified=rep(FALSE,s)    
    
    for (i in 1:l[1]) {    
        found=attribute==att_tabled[i]
        classified[found]=TRUE
    }    
    
    ##############################################################
    ## Classify the rest, whilst adding the new values to the   ##
    ## lookup table                                             ##
    ##############################################################
    indexs=unique(attribute[classified==FALSE])
    
    if (length(indexs)>0) {
        p=l[1];
        for (i in indexs) {
            if (is.na(i) || i=='NA') {
                next
            }
            
            p=p+1        
            lookup[p,]=0
            lookup=rename_row(lookup,p,i)
            test_more=TRUE
            
            print(paste("what classification is: ",i,"?",sep=""))  
            cat(choices_display)        
            
            while(test_more) {
                no=as.numeric(readline(
                    "enter corrisponding number(0 for no more): "))
                            
                no-test_number_enterted(no,nchoices,0)
                if (no==0) {
                    test_more=FALSE
                } else {
                    lookup[p,no]=1
                }
            }
        }
        
        write.csv(lookup,filename_lookup)
    }
    
    if (return_new_table) {
        return(lookup)
    }
}

test_number_enterted <-function(no,maxno,minno) {
    test=TRUE
    
    while (test) {
        if (is.numeric(no)==FALSE) {
            no=readline("not a number. try again: ")
        } else if (no>maxno) {
            no=readline("number too large. try again: ")
        } else if (no<minno) {
            no=readline("number too small. try again: ")
        } else {
            test=FALSE
        }
    }
    
    return(no)
}

rename_row <-function(dataf,p,name) {
    a=dim(dataf)
    if (p==a[1]) {
        row.names(dataf)=c(row.names(dataf[1:(p-1),]),name)
    } else {
        row.names(dataf)=c(row.names(dataf[1:(p-1),]),name,
                           row.names(dataf[p+1:a[1],]))
    }
    
    return(dataf)
}

