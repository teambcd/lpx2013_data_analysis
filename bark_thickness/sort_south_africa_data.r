configure <- function() {
    filename_data <<- "outputs/South_africa_data2.csv"
    filename_location <<- "data/South_africa_locations.csv"
    filename_species <<- "data/South_africa_species.csv"
    
    bdata <<- read.table(filename_data,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    a <<- dim(bdata)
    
}

average_out_same_inds <- function()
{
    configure()
    
    test_done=is.na(bdata$Species.Sample.Code)
    
    odata=bdata[which(test_done),]
    
    b=dim(odata)    
    p=b[1]
    
    for (i in which((test_done)==FALSE)) {
        if (test_done[i]) {
            next
        }
        p=p+1
        code=bdata$Species.Sample.Code[i]
        indexs=which(bdata$Species.Sample.Code==code)
        
        DBH_mean=mean(bdata$tree.diameter..mm.[indexs])
        BT_mean=mean(bdata$bark.thickness..mm.[indexs])
        
        test_done[indexs]=TRUE
        
        odata[p,]=bdata[i,]
        odata$tree.diameter..mm.[p]=DBH_mean
        odata$bark.thickness..mm.[p]=BT_mean        
    }
    
    write.csv(odata,filename_data,row.names=FALSE)
}
        
       
fill_in_location_lat_lon_from_site_name <- function()
{
    configure()
    
    ldata=read.csv(filename_location)
    
    b=dim(ldata)
    
    for (i in 1:b[1]) {
        indexs=which(bdata$lat==ldata$Location[i])
        bdata$lat[indexs]=ldata$Latitude[i]
        bdata$lon[indexs]=ldata$Longitude[i]
    }
        
    write.csv(bdata,filename_data,row.names=FALSE)
}

fill_in_species_attributes <-function() {

    configure()
    source("Allocate_rstypes.r")
    
    sdata=read.csv(filename_species)
    
    b=dim(sdata)
    RStype=matrix('NaN',a[1])
    
    for (i in 1:b[1]) {
        test=(bdata$Genus==sdata$Genus[i])+
            (bdata$Species==sdata$Species[i])
        indexs=which(test==2)
        bdata$Climate.range[indexs]=as.character(sdata$Climate.limits[i])
        bdata$phenology[indexs]=as.character(sdata$Phenology[i])
        bdata$life.form[indexs]=as.character(sdata$Growth.form[i])  
        
        if (sdata$Resprouting[i]=='Non-sprouter') {
             bdata$reprouter.[indexs]='No'
        } else {
            bdata$reprouter.[indexs]='Yes'
            
            RStype[indexs]=as.character(sdata$Resprouting[i])
        }
    }
   
    RSmatrix=matrix(0,length(RStype),5)

    c(RStype,RSmatrix):=allocate_RStype(RStype,RSmatrix)         
    RStype=allocate_unallocated_RS(RStype)    
    c(RStype,RSmatrix):=allocate_RStype(RStype,RSmatrix)

    bdata$Apical=RSmatrix[,1]
    bdata$Epicormic=RSmatrix[,2]
    bdata$Basal.Collar=RSmatrix[,3]
    bdata$Underground=RSmatrix[,4]

    write.csv(bdata,filename_data,row.names=FALSE)
}