configure <- function () {
    source("../libs/return_multiple_from_functions.r")
    source("convert_species_list_to_pfts.r")
    source("split_data_for_plotting.r")
    source("write_species_to_pft_table.r")
    filename_data <<- 'data/data.csv'
    filename_species <<- 'data/species_list.csv'
    
    plot_data <<- TRUE
    write_pft_table <<- FALSE
}


classify_data_based_on_species_list <- function () {

    configure()
    
    pdata=read.table(filename_data,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
      
    sdata=read.table(filename_species,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
        
        
    sdata=convert_species_list_to_pfts(sdata,TRUE,TRUE)
    
    
    odata=data.frame(DBH=pdata$treediameter..mm.,
                     BT =pdata$bark.thickness..mm.,
                     pft='U',stringsAsFactors=FALSE)
         
    a=dim(sdata) [1]
    #browser()
    for (i in 1:a) {
        print(i/a)
        
        genus=sdata$Genus[i]
        species=sdata$Species[i]
        test=((pdata$Genus==genus)+(pdata$Species==species))==2
        odata$pft[test]=sdata$pft[i]
    }
    odata=odata[complete.cases(odata),]
    
    if (plot_data) {
        split_data_for_plotting(odata,TRUE)
    }
    
    if (write_pft_table) {
        write_species_to_pft_table(sdata)
    }

}