configure_split_data_for_plotting <- function() {
    source("plot_all_QR_BT.r")
    source("libs/simple_add_or_find_in_lookup_table.r")
    plot_def_table <<- 'system_files/pft_to_plot.csv'
    
}


split_data_for_plotting <- function(pdata,plot_data=TRUE) {
    configure_split_data_for_plotting()
    
    plots=simple_add_or_find_in_lookup_table(pdata$pft,
        plot_def_table,return_new_table=TRUE)
    
    split_data=list() 
    
    for (i in colnames(plots) ) {
        print(i)
        pfts=rownames(plots[plots[[i]]==1,])
        ID=rep(0,length(pdata$pft))
        for (j in pfts) {
            ID=ID+(pdata$pft==j)
        }
        ID=ID>0
        if (sum(ID)>0) {
            split_data[[i]]=pdata[ID,]
        }
    }
    
    p1=plots[2,]
    p2=plots[3,]
    
    if (plot_data) {
        plot_all_QR_BT(split_data,
            origonal_param1=p1,origonal_param2=p2)
    }
}