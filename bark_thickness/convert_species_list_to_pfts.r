configure_convert_species_list_to_pfts <- function(species_list=NaN) {
    filename_in <<- 'data/species_list.csv'
    
    climate_pft_lookup      <<- 'system_files/climate_pft_lookups.csv'
    leaf_pft_lookup         <<- 'system_files/leaf_pft_lookups.csv'
    phenology_pft_lookup    <<- 'system_files/phenology_pft_lookups.csv'
    life_form_pft_lookup    <<- 'system_files/life_pft_lookups.csv'   
    resprout_pft_lookup     <<- 'system_files/rs_pft_lookups.csv'   
    
    filename_out            <<- 'outputs/species_in_pfts.csv'
    
    ## Open up files
    if (is.null(dim(species_list))) {
        species_list<<- read.table(filename_in,
            sep=",",header=TRUE,stringsAsFactors = FALSE)        
    }
        
    climate_pft     <<- read.table(climate_pft_lookup,
            sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    leaf_pft        <<- read.table(leaf_pft_lookup,
            sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    phenology_pft   <<- read.table(phenology_pft_lookup,
            sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    life_pft        <<- read.table(life_form_pft_lookup,
            sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    resprout_pft    <<- read.table(resprout_pft_lookup,
            sep=",",header=TRUE,stringsAsFactors = FALSE)
    
}

convert_species_list_to_pfts <- function(species_list=NaN,
    write_conversion=TRUE,abbriviate=FALSE) {
    
    configure_convert_species_list_to_pfts(species_list)
    
    species_list$Climate.range=
        classify_species_to_pfts(species_list$Climate.range,
        climate_pft,climate_pft_lookup)
        
    species_list$leaf.type=
        classify_species_to_pfts(species_list$leaf.type,
        leaf_pft,leaf_pft_lookup)
        
    species_list$phenology=
        classify_species_to_pfts(species_list$phenology,
        phenology_pft,phenology_pft_lookup)
        
    species_list$life.form=
        classify_species_to_pfts(species_list$life.form,
        life_pft,life_form_pft_lookup)
        
    species_list$reprouter=
        classify_species_to_pfts(species_list$reprouter,
        resprout_pft,resprout_pft_lookup)
    
    if (abbriviate) {
        species_list=convert_to_pft_code(species_list)
    }
    
    if (write_conversion)    {
        write.csv(species_list,filename_out)
    }
    
    return(species_list)
        
}

classify_species_to_pfts <- function(species,lookup,filename_lookup) {
    
    choices=colnames(lookup)[-1]
    
    nchoices=0
    choices_display=""
    for (i in choices) {
        nchoices=nchoices+1
        choices_display=paste(choices_display,nchoices,".","\t",i,"\n")
    }
    
    ## initial classification
    l=dim(lookup)
    s=length(species)
    
    classified=rep(FALSE,s)    
    species
    
    for (i in 1:l[1]) {
        name=choices[which(lookup[i,2:l[2]]==1)[1]]        
        found=species==lookup[i,1]
        species[found]=name
        classified[found]=TRUE
    }
    
    ## classify the rest
    indexs=which(classified==FALSE)
    
    indexs=unique(species[indexs])
    
    p=l[1];
    for (i in indexs) {
        if (is.na(i) || i=='NA') {
            found=species==i
            species[found]='unclassified'
            next
        }
        print(paste("what classification is: ",i,"?",sep=""))  
        cat(choices_display)
        no=as.numeric(readline("enter corrisponding number: "))
        if (is.numeric(no)==FALSE) {
            no=readline("not a number. try again: ")
        } else if (no>nchoices) {
            no=readline("number too large. try again: ")
        } else if (no<1) {
            no=readline("number too small. try again: ")
        }  
        name=choices[no]

        p=p+1
        lookup[p,1]=i
        lookup[p,2:(nchoices+1)]=0
        lookup[p,no+1]=1
        
        found=species==i
        species[found]=name
    }
    
    write.csv(lookup,filename_lookup,row.names=FALSE)
    
    return(species)
}

convert_to_pft_code <- function(species_list) {
    configure_convert_species_list_to_pfts(species_list)
    
    
     
    climate_pft=climate_pft[1,][-1]
    leaf_pft=leaf_pft[1,][-1]
    phenology_pft=phenology_pft[1,][-1]
    life_pft=life_pft[1,][-1]
    resprout_pft=resprout_pft[1,][-1]
    
    species_list$Climate.range=
        abbribiate_attribuite(species_list$Climate.range,climate_pft)
        
    species_list$leaf.type=
        abbribiate_attribuite(species_list$leaf.type,leaf_pft)
        
    species_list$phenology=
        abbribiate_attribuite(species_list$phenology,phenology_pft)
        
    species_list$life.form=
        abbribiate_attribuite(species_list$life.form,life_pft)
        
        
    species_list$reprouter=
        abbribiate_attribuite(species_list$reprouter,resprout_pft)
    
    species_list$reprouter=assign_RS_type(species_list)
    
    species_list$reprouter[species_list$reprouter=='U']='XXX'
    pfts=paste(species_list$Climate.range,species_list$leaf.type,
        species_list$phenology,species_list$reprouter,sep="")
        
    pfts[grepl('U',pfts)]='U'
    
    pfts=gsub('XXX','U',pfts)
    
    pft_list=data.frame(Genus=species_list$Genus,Species=species_list$Species,
                        pft=pfts,stringsAsFactors=FALSE)
    return(pft_list)
    
}

abbribiate_attribuite <- function(attribute,abbriviation) {
    for (name in colnames(abbriviation)) {
        attribute[attribute==name]=abbriviation[name]
    }
    
    return(attribute)
}


assign_RS_type <-function(species_list) {

    RStype=rep("U",dim(species_list)[1])
    
    RS_test=species_list$reprouter=='NR'
    RStype[RS_test]="N"
    
    RS_test=species_list$reprouter=='RS'
    RStype[RS_test]="R"
    
    Ground_test=(species_list$Underground)>0
    RStype[Ground_test]='B'
    
    Ground_test=(species_list$Basal.Collar)>0
    RStype[Ground_test]='G'
    
    Arial_test=(species_list$Epicormic)>0
    RStype[Arial_test]='E'
    
    Arial_test=(species_list$Apical)>0
    RStype[Arial_test]='A'
    
    
    
    return(RStype)
}
    
        