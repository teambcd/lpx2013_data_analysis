configure_write_species_to_pft_table <- function() {
    source("../libs/return_multiple_from_functions.r")
    source("libs/simple_add_or_find_in_lookup_table.r")
    
    filename_out <<- 'outputs/condensed_species_list_NR.csv'
    plot_def_table <<- 'system_files/pft_to_plot.csv'
    
    group_to_Arial  <<- FALSE
    no_RS_grouping  <<- TRUE
    plot_def_table_g2A <<- 'system_files/pft_to_Arial_plot.csv'
}

write_species_to_pft_table <- function(pdata) {
    configure_write_species_to_pft_table()
    if (group_to_Arial) {
        plot_def_table=plot_def_table_g2A
    }
    
    if (group_to_Arial) {  
        pdata[,3]=group_to_AR_OR_NR(pdata[,3])
    }
    
    if (no_RS_grouping) {  
        pdata[,3]=group_to_nothing(pdata[,3])
    }
    
    plots=simple_add_or_find_in_lookup_table(pdata$pft,
        plot_def_table,return_new_table=TRUE)
        
    species=paste(pdata[,1],pdata[,2])
    apft=pdata[,3]
    
     
    plots[1:3,]=0.5
    a=dim(plots)
    empty_df=rep("",a[1]*a[2])

    output_table=data.frame(LPX.pft=empty_df,
                            pft=empty_df,
                            species=empty_df,
                            stringsAsFactors = FALSE)
    
    p=1
    for (i in colnames(plots)) {
        output_table$LPX.pft[p]=i
        
        pfts=rownames(plots[(plots[[i]]==1),])
        
        for (j in pfts) {
            output_table$pft[p]=j
            output_table$species[p]=
                make_species_list_as_string(apft,j,species)           
            p=p+1
        } 
    }
    
    ## do undefineds
    
    output_table$LPX.pft[p]='Unclassified'
    for (i in 1:a[1]) {
        if (sum(plots[i,])==0) {
            output_table$pft[p]=rownames(plots)[i]
            output_table$species[p]=make_species_list_as_string(apft,rownames(plots)[i],
            species)
            p=p+1
        }
    }
            
            
   
    output_table=output_table[1:(p-1),]
    write.csv(output_table,filename_out)

}

group_to_AR_OR_NR <- function(apft) {
    
    classified=which(nchar(apft)>1)
    ipft=apft[classified]
    RS=substr(ipft,nchar(ipft),nchar(ipft))
    
    AR=(RS=='A')+(RS=='E')
    NR=(RS=='N')+(RS=='U')    
    OR=(AR==0)+(NR==0)
    
    AR=which(AR>0)
    NR=which(NR>0)
    OR=which(OR==2)
    
    substr(ipft[AR],nchar(ipft[AR]),nchar(ipft[AR]))='A'
    substr(ipft[NR],nchar(ipft[NR]),nchar(ipft[NR]))='N'
    substr(ipft[OR],nchar(ipft[OR]),nchar(ipft[OR]))='O'
    
    apft[classified]=ipft
    
    return(apft)
}

group_to_nothing <-function(apft) {
    
    classified=which(nchar(apft)>1)
    ipft=apft[classified]
    substr(ipft,nchar(ipft),nchar(ipft))='Z'
    
    apft[classified]=ipft    
    return(apft)
}

make_species_list_as_string <-function(apft,j,species) {
     species_list=""
    for (k in which(apft==j)) {
        species_list=paste(species_list,species[k],", ",sep="")        
    }
    
    return(species_list)
}