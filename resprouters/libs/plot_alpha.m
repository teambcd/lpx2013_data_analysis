function plot_alpha(filename,varname,limits,axis_range,colour)

    data=open_netcdf_variable(0,filename,varname,0,0);
    [~,b]=size(data);
    data=data(:,b:-1:1);
    data(data<0)=0;
    
    
    [lat lon]=open_lat_lon(filename,'latitude','longitude');

    cf=0.15;
    lat(1:2)=lat(1:2)-cf;
    lon(1:2)=lon(1:2)+cf;

    load mask2
    load aus_land_shape
    plot_with_lims_dres_cline11(lat,lon,...
            data,'same window',...
            'colour',{colour},...
            'limits',limits,...
            'line thickness',2.5,...
            'boundry lines',landmask_vector,...
            'xtick',[-180 180],'ytick',[-90 90],...
            'turn off find missing values',...
            'boundry line width',1,'axis',axis_range); 

end