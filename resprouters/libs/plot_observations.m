function [RS_NR RS_NR_colour]=plot_observations(filename_RS,axis_range,limits_RS,colours)

data=importdata(filename_RS);
rows=data.textdata(2:size(data.textdata,1),1);
cols=data.textdata(:,1);
data=data.data;

in_range=((data(:,1)>axis_range(3))+(data(:,1)<axis_range(4))+...
    (data(:,2)>axis_range(1))+(data(:,2)<axis_range(2)))==4;

rows=rows(in_range);
data=data(in_range,:);

sites=unique(rows); nsites=size(sites,1);

RS_NR=zeros(nsites,3,'single');
RS_NR_colour=zeros(nsites,3,'single');

for i=1:nsites
    i/nsites
    %find entries for this site
    s= ismember(rows,char(sites{i}));
    sdata=data(s,:);
    
    %lat and lon
    RS_NR(i,1)=sdata(1,1);
    RS_NR(i,2)=sdata(1,2);
    
    %calculate % RS
    RS=(((sdata(:,4)==1)+(sdata(:,5)==1))>0);
    RS=sum(sdata(RS,13));
    
    NR=(((sdata(:,8)==1)+(sdata(:,8)==1)+(sdata(:,8)==1))>0);
    NR=sum(sdata(NR,13));
    
    RS_NR(i,3)=100*RS/(RS+NR);
    
    %calculate limits
    lim_test=limits_RS>RS_NR(i,3);
    if sum(lim_test)==0
        lim=size(RS_NR(i,3),2)+1;
    else
        [~,lim]=max(lim_test);
    end
    if lim==0
        RS_NR_colour(i,:)=colours(lim,:);
    else
        RS_NR_colour(i,:)=colours(lim+3,:);
    end
end

%figure; scatter(RS_NR(:,2),RS_NR(:,1),50,[0.5 0.5 0.5],'filled')

figure; area(axis_range(1:2), axis_range([3 3]), axis_range(4), 'FaceColor', [.9 .9 .9]);
%area(axis_range(1:2),axis_range([3 3]))
hold on; scatter(RS_NR(:,2),RS_NR(:,1),50,RS_NR_colour)
axis(axis_range)

load aus_land_shape
plot(landmask_vector(1:40,4),landmask_vector(1:40,5),'k','LineWidth',2)
plot(landmask_vector(41:600,4),landmask_vector(41:600,5),'k','LineWidth',2)

