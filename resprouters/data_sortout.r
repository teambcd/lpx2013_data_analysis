combined_burnt_and_control_sites <- function() {
    
    source("libs/find_diff_from_lines_described_by_points.r")
    source("../libs/install_and_source_library.r")
    install_and_source_library("TTR")
    
    filename='data/data_what_needs_combining.csv'
    filename_out='outputs/data_what_needs_combining.csv'
    
    obs=read.csv(filename)
    
    sites=1:max(obs$Site,na.rm=TRUE)
    
    #obso=sapply(sites,sort_for_diffing,obs)
    
    obso=c()
    
    for (i in sites) {
        obsi=sort_for_diffing(i,obs)
        if (length(obsi)>1) {
            obso=cbind(obso,t(obsi))
        }
    }
    
    write.csv(t(obso),filename_out)
    
}

sort_for_diffing <- function(site,obs) {
    obsi=obs[obs$Site==site,]
    
    if (length(obsi$Year)<2) {
        return(NaN)
    }
    
    years=seq(min(obsi$Year),max(obsi$Year),1/12)
    Cndvi=obsi[is.na(obsi$Burnt),c(2,3)]
    Bndvi=obsi[is.na(obsi$Control),c(2,4)]

    Qndvi=find_diff_from_lines_described_by_points(Bndvi,Cndvi,years,12)
    print(site)
    return(cbind(site,Qndvi))
 }   


#################################################

do_moving_average <- function(rmean=12) {
  install_and_source_library("TTR")
  obs=read.csv('data/needs12av.csv')
  
  x=seq(min(obs[,1]),max(obs[,1]),1/12)
  y=approx(obs[,1],obs[,2],x)$y
  
  y=SMA(y,rmean)[rmean:length(y)]
  x=x[7:(length(y)+6)]
  
  xy=cbind(x,y)
  
  write.csv(xy,'outputs/temp.csv')
}
