##################################################################
## Configuration                                                ##
##################################################################
configure_plot_abundances <-function() {
    
    source("../libs/make_transparent.r")
    source("../libs/install_and_source_library.r")
    source("../libs/dev.off.all.r")
    source("../libs/return_multiple_from_functions.r")
    install_and_source_library("RNetCDF")
    install_and_source_library("stats")
    
    dev.off.all()
    ##Observations
    obs_filename        <<- "../../Resprouting_workshop/outputs/Site_RS_coverT2.csv"
    obs_remote_filename <<- "../../benchmark-system/bench_data/veg_cont_fields_CRU.nc"
    obs_alpha_filename  <<- "data/low_res_alpha.nc"
    obs_crop_filename   <<- "../../Inputs/Historical/cropland_18512006.nc"
    obs_pas_filename    <<- "../../Inputs/Historical/pasture_18512006.nc"
    
    obs_tree_varname    <<- "Tree_cover"
    obs_grass_varname   <<- "Herb"
    obs_alpha_varname   <<- "alpha"
    obs_crop_varname    <<- "crop"
    obs_pas_varname     <<- "pas"
    
    crop_yr             <<- 150
    
    ##  Modelled data:
    LPX_fpc_varname     <<- 'fpc_grid'
    LPX_fpc_filename    <<- "../../Outputs/LPD/origonal/benchmarking_fire_fapar_fpc_npp_height_6762dd_r184_Aus-10147.nc"
    LPXM_fpc_filename   <<- "../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10147.nc"
   
    LPX_alpha_varname   <<- 'alpha_ws'
    LPX_alpha_filename  <<- "test_spinup_nn3.1-2155.nc"
    
    
    ## Plot what
    Include_grass       <<- TRUE
    Include_FA          <<- FALSE
    Include_crop        <<- FALSE
   
    ## Plot info
    startyr             <<- 10147
    nyrs                <<- 9
    
    xlabel_name         <<- expression(paste(alpha," (Actual/Potential ET)",sep=""))            
    ylabel_name         <<- 'Normalised %';  
    alpha               <<- 0.995
    figure_name         <<- 'figs/RS_abundance.png'
    figure_height       <<- 10
    figure_width        <<- 6
    
    x_range             <<- c(0,1)
        
    nbins               <<- 100
    npolygons           <<- 100
    bin_width           <<- 0.1
    
    tree_colour         <<- "darkgoldenrod4"
    tree_crop_colour    <<- "burlywood3"
    grass_colour        <<- "darkgreen"
    grass_crop_colour   <<- "green"
    RS_colour           <<- "red"
    NR_colour           <<- "black"
    FA_colour           <<- "blue"
    

}

##################################################################
## Main function                                                ##
##################################################################

plot_abundances <- function() {
    configure_plot_abundances()
    
    ## set up window
    set_up_window()
    
    set_and_plot_remote_observations()
    set_and_plot_LPX0_simulation(LPX_fpc_filename,"b) LPX") 
    add_cropland(LPX_fpc_filename,1:7)    
    set_and_plot_LPX0_simulation(LPXM_fpc_filename,"c) LPX-Mv1-rs")
    add_cropland(LPXM_fpc_filename,c(1:7,10:13))
    set_and_plot_RS_bds_observations()
    set_and_plot_LPXM_simulation(FALSE)
    add_y_label()
    dev.off()
}
##################################################################
## Gubbins                                                      ##
##################################################################
set_and_plot_remote_observations <- function () {

    if (Include_grass) {
        set_up_plot(c(tree_colour,grass_colour),
            c("tree","grass"),xaxt="n")
    } else {
        set_up_plot(tree_colour,c("tree"),
            xaxt="n")
    }
    plot_observations()
    mtext('a) Observed - VCF',side = 3,line=-2,adj=0.01,cex=1)

}

add_cropland <- function(LPX_filename,tree_pfts=1:7) {
    
    if (Include_crop==FALSE) {
        return()
    }
    
    alpha=load_LPX_data(LPX_alpha_filename,LPX_alpha_varname)
        
    crop=load_LPX_data(obs_crop_filename,
        obs_crop_varname)[,,crop_yr]
        
    pas=load_LPX_data(obs_pas_filename,
        obs_pas_varname)[,,crop_yr]
    
    years=startyr+(1:nyrs)-1    
    c(nn1,tree,grass):=sapply(years,load_LPX_cover_data,
        LPX_filename,LPX_fpc_varname,
        c(1,2),tree_pfts,c(8,9))
        
    rm(nn1)
    grass=grass/100
    grass=grass*(1-(crop+pas))+crop+pas
    tree=tree*(1-(crop+pas))+crop+pas
    #crop=crop*100

    test=is.na(alpha)+is.na(crop)+is.na(grass)+is.na(tree)
    test=test==0
    alpha=as.vector(alpha[test])
    grass=as.vector(grass[test])
    tree=as.vector(tree[test])
    
    line_shade_plot(grass,alpha*1.26,grass_crop_colour)
    line_shade_plot(tree,alpha*1.26,tree_crop_colour)
}

set_and_plot_RS_bds_observations <- function() {
    
    if (Include_FA) {
        set_up_plot(c(RS_colour,NR_colour,FA_colour),
            c("RS","NR","FA"),xaxt="n",plot_clark=FALSE)
    } else {
        set_up_plot(c(RS_colour,NR_colour),c("RS","NR"),
            xaxt="n",plot_clark=FALSE)
    }
    
    plot_RS_observations()
    mtext('d) Observed - ',side = 3,
        line=-2,adj=0.01,cex=1)
        
    mtext('     RS database',side = 3,
        line=-3.33,adj=0.0,cex=1)

}

set_and_plot_LPX0_simulation <- function(filename,mtitle="c) LPX") {

    if (Include_grass) {
        set_up_plot(c(tree_colour,grass_colour,"white","white"),
            c("tree","grass"," "," "),xaxt="n")
    } else {
        set_up_plot(c(tree_colour,"white","white"),
            c("tree"," "," "),xaxt="n")
    }        
    plot_LPX_runs(filename)
    mtext(mtitle,side = 3,line=-2,adj=0.01,cex=1)

}

set_and_plot_LPXM_simulation <- function(Include_grass=TRUE) {

    if (Include_grass) {
        set_up_plot(c(RS_colour,NR_colour,grass_colour,"white","white"),
            c("RS","NR","grass"," "," "),plot_clark=FALSE)
    } else {
        set_up_plot(c(RS_colour,NR_colour,"white","white"),
            c("RS","NR"," "," "),plot_clark=FALSE)
    }        
    plot_LPXM_runs(Include_grass)
    add_x_label()
    mtext('e) LPX-Mv1rs',side = 3,line=-2,adj=0.01,cex=1)

}

##################################################################
## Set up overall window                                        ##
##################################################################   
set_up_window <- function() {
    png(figure_name, height=figure_height, width=figure_width,
        units = "in",res=300)
    par(oma=c(2,4,2,2),mar=c(0,0,2,0))
    layout(1:6,heights=c(1,1,1,1,1,0.2))

}   

##################################################################
## Scripts for setting plots                                    ##
##################################################################
set_up_plot <- function(plot_colours,label,plot_clark=FALSE,...) {
        
    plot(x_range,c(0,100),xaxs='i',yaxs='i',
    xlim=x_range,ylim=c(0, 100),
        xlab=parse(text=xlabel_name),ylab=ylabel_name,
        type = "n", cex=.5,...) 
    
    
    ## Clark et al RS location
    if (plot_clark) {
        pcloc=108
        lines(c(0.522,0.923),c(pcloc,pcloc),lwd=2, xpd=T)
        points(c(0.522,0.923),c(pcloc,pcloc),cex=2,pch=19, xpd=T)
    }
    
    axis(1,labels=FALSE)
    add_legend(plot_colours,label)
    
}

add_y_label <- function() {
    mtext(ylabel_name,side = 2,line=2.5,adj=3.5,cex=0.8)
}

add_x_label <- function() {
    mtext(parse(text=xlabel_name),side = 1,line=3,adj=0.5,cex=0.8)
}

add_legend <- function(plot_colours=c('red','blue','black'),
    label=c('    RS', '    OS','    NR')) {
    
    quant_colours <- make.transparent(plot_colours,alpha)
    
    standard_legend(quant_colours,lwd=0,label=label)
    
    label=rep(' ',length(label))
    for (lwd in seq(1,25,25/100)) {
        standard_legend(quant_colours,lwd=lwd,label=label)
    }
    standard_legend(plot_colours,lwd=2,label=label)
}

standard_legend <- function(coloursz,lwd=1,lty=1,
    label=c(' ',' ',' ')) {

  legend(x=.1,y=35,legend=label,
         border=FALSE,col=coloursz,bty="n",xpd=TRUE,
         lwd=lwd,lty=lty)
}

##################################################################
## Sets up for plotting the lines and shades areas for          ##
##      each plot                                               ##
##################################################################
plot_observations <- function(cal_yr,use_index=FALSE) {
    
    tree=load_LPX_data(obs_remote_filename,obs_tree_varname)
    grass=load_LPX_data(obs_remote_filename,obs_grass_varname)    
    alpha=load_LPX_data(obs_alpha_filename,obs_alpha_varname,
        flip=TRUE)
        
    alpha=1.26*load_LPX_data(LPX_alpha_filename,LPX_alpha_varname,
        TRUE,FALSE)
   
    test=is.na(tree)+is.na(alpha)
    test=test==0
        
    tree=as.vector(tree[test])
    #tree[tree>79.99]=92.5
    #tree[tree<20.01]=12.5
    grass=as.vector(grass[test])
    alpha=as.vector(alpha[test])
    
    #grass[grass>79.99]=92.5
    #grass[grass<20.01]=12.5
    
       
    if (Include_grass) {   
        line_shade_plot(tree,alpha,tree_colour,use_index=use_index) 
        line_shade_plot(grass,alpha,grass_colour,use_index=use_index)#,ycutoff=c(20,80))
    } else {
        line_shade_plot(tree,alpha,tree_colour,use_index=use_index)#,ycutoff=c(20,80)) 
    }
}

plot_RS_observations <- function(cal_yr) {
    
    obs=read.csv(obs_filename)
    AR=obs$Arial.RS
    OA=obs$Other.RS
    NR=obs$None.RS
    tot=(AR+OA+NR)/100

    xcomp=obs$alpha
    
    line_shade_plot(AR/tot,xcomp,RS_colour,lty=1)
    if (Include_FA) {
        line_shade_plot(OA/tot,xcomp,FA_colour,lty=1)
    }
    line_shade_plot((NR+OA)/tot,xcomp,NR_colour,lty=1)
}


plot_LPX_runs <-function(filename,use_index=FALSE) {
    
    years=startyr+(1:nyrs)-1
    
    TR=matrix(0,720,360)
    GS=TR
    fire=TR
    
    for (i in years) {
        c(TR0,GS0,nn):=load_LPX_cover_data(i,
            filename,LPX_fpc_varname,1:7,8:9,1:9)
        TR=TR+TR0/nyrs
        GS=GS+GS0/nyrs
        fire=fire+load_LPX_fire_data(i,filename)/nyrs
    }    
    alpha=load_LPX_data(LPX_alpha_filename,LPX_alpha_varname)
    #alpha=load_LPX_data(obs_alpha_filename,obs_alpha_varname,
    #    TRUE,TRUE)

    test=is.na(TR)+is.na(GS)+is.na(alpha)
    test=test==0
    TR=as.vector(TR[test])
    GS=as.vector(GS[test])
    fire=as.vector(fire[test])
    GS=GS/(1-fire)
    alpha=1.26*as.vector(alpha[test])

    if (Include_grass) {
        line_shade_plot(GS,alpha,grass_colour,use_index=use_index)
    }
    line_shade_plot(TR,alpha,tree_colour,use_index=use_index)
    
}

plot_LPXM_runs <-function(Include_grass=TRUE) {
    
    years=startyr+(1:nyrs)-1
    
    #c(RS,NR,GS):=sapply(years,load_LPX_cover_data,
    #    LPXM_fpc_filename,LPX_fpc_varname,
    #    c(10,11,12,13),c(1:7),c(8,9))
        
    RS=matrix(0,720,360)
    NR=RS
    GS=RS
    
    for (i in years) {
        c(RS0,NR0,GS0):=load_LPX_cover_data(i,
            LPXM_fpc_filename,LPX_fpc_varname,10:13,1:7,8:9)
        RS=RS+RS0/nyrs
        NR=NR+NR0/nyrs
        GS=GS+GS0/nyrs
    }   

    alpha=load_LPX_data(LPX_alpha_filename,LPX_alpha_varname)
 
    test=is.na(RS)+is.na(NR)+is.na(alpha)
    test=test==0
    RS=as.vector(RS[test])
    NR=as.vector(NR[test])
    GS=as.vector(GS[test])
    tot=(RS+NR)/100
    alpha=1.26*as.vector(alpha[test])
    
    if (Include_grass) {
        line_shade_plot(GS,alpha,grass_colour)
    }
    line_shade_plot(RS/tot,alpha,RS_colour)
    line_shade_plot(NR/tot,alpha,NR_colour)
    
}


##################################################################
## Plot the line and shaded area                                ##
##################################################################

line_shade_plot <- function(Y,X,colour,ycutoff=NULL,use_index=FALSE,
    lwd=2,...) {
    c(x,y,e):=binning(Y,X) 

    if (use_index) c(y,nn):=abundance_index(y,e)

    e[is.na(e)]=0
    test=which(is.na(y)==FALSE)
    x=x[test]
    y=y[test]
    e=e[test,]
    
    lines(x,y,col=make.transparent(colour,0.3),lwd=2,...)
    
    x=c(x,rev(x))
    
    ii=round(npolygons/2)+1
    
    for (i in (round(npolygons/2)+1):npolygons) {
        ii=ii-1

        yp=c(e[,i],rev(e[,ii]))
        polygon(x,yp,col=make.transparent(colour,0.993),border=NA)
    }
    polygon(x,yp,col=make.transparent(colour,0.97),border=NA)
    if (is.null(ycutoff)==FALSE) {
        mask_upper_or_lower_plot(x,y,ycutoff[1],FALSE)
        mask_upper_or_lower_plot(x,y,ycutoff[2],TRUE)
    }
}

binning <- function(Y,X) { 

    dbin=(x_range[2]-x_range[1])/nbins
    bins=seq(x_range[1],x_range[2],dbin)
    ymean=rep(NaN,nbins)
    yerr=matrix(NaN,nbins,npolygons)
    
    for (i in 1:nbins) {
        r1=bins[i]-bin_width/2
        r2=bins[i+1]+bin_width/2
        test=(X<r2)+(X>r1)
        
        if (sum(test==2,na.rm=TRUE)>3) {
            ymean[i]=mean(Y[test==2],na.rm=TRUE)
            
            yerr[i,]=quantile(Y[test==2],
                probs=seq(0.1,0.9,length.out=npolygons),na.rm=TRUE)
        }
    }
    x=bins[1:nbins]+dbin/2
    return(list(x,ymean,yerr))

}

mask_upper_or_lower_plot <- function(x,y,ycutoff,up_not_down=TRUE) {
    x=c(min(x,na.rm=TRUE),max(x,na.rm=TRUE),max(x,na.rm=TRUE),min(x,na.rm=TRUE))
    
    if (up_not_down) {
        maxy=max(y,na.rm=TRUE)
    } else {
        maxy=min(y,na.rm=TRUE)
    }
    y=c(ycutoff,ycutoff,maxy,maxy)
    dy=(maxy-ycutoff)/100

    for (i in 1:100) {
        y[1:2]=y[1:2]+dy
        polygon(x,y,col=make.transparent('white',0.97),border=NA)
    }
}



##################################################################
## Loading netcdf data                                          ##
##################################################################

load_LPX_cover_data <-function(year,filename,varname,
    G1_pfts,G2_pfts,G3_pfts) {

    print(paste("   opening year:",year))
    n=nchar(filename)
    substr(filename,n-7,n-3)= as.character(year)
    print(filename)
    fpc=load_LPX_data(filename,varname)
    
    G1=rowSums(fpc[,,G1_pfts],dim=2)*100
    G2=rowSums(fpc[,,G2_pfts],dim=2)*100
    G3=rowSums(fpc[,,G3_pfts],dim=2)*100

    return(list(G1,G2,G3))

}
    
load_LPX_fire_data <- function(year,filename) {
    fire=load_LPX_data(filename,'mfire_frac',yr=year)
    return(rowSums(fire,dim=2))
}

load_LPX_data <-function(filename,varname,swap=FALSE,flip=FALSE,
    yr=NULL) {
    
    if (is.null(yr)==FALSE) {
        filename=construct_filename(filename,yr)
    }

    nc=open.nc(filename)
    vdata=var.get.nc(nc, varname)
    
    if (swap) {
        vdata=swap_halves(vdata,yr)
    }
    if (flip) {
        vdata=flip_data(vdata,yr)
    }
    
    return(vdata)

}

construct_filename <- function(filename,year) {
    n=nchar(filename)
    substr(filename,n-7,n-3)= as.character(year)
    print(filename)
    return(filename)
}

swap_halves <- function(vdata,yr=1) { 
        
        vdata=extract_dim(vdata,yr)

        a=dim(vdata)
        vdatai=vdata
        vdata[1:floor(a[1]/2),]=vdatai[(ceiling(a[1]/2)+1):a[1],]
        vdata[(floor(a[1]/2)+1):a[1],]=vdatai[1:ceiling(a[1]/2),]
        return(vdata)
}

flip_data <- function(vdata,yr=1) {
    extract_dim(vdata,yr)

    a=dim(vdata)
    vdata[,a[2]:1]=vdata[,1:a[2]]
    return(vdata)
}

extract_dim <- function(vdata,yr) {

    if (length(dim(vdata))==3) {
        vdata=vdata[,,yr]
    }
    
    return(vdata)
}

##################################################################
## Converting cover to index                                    ##
##################################################################

abundance_index <- function(cover,error) {
    min_cover=min(cover,na.rm=TRUE)    
    cover=cover-min_cover
    error=error-min_cover
    
    max_cover=max(cover,na.rm=TRUE)
    cover=100*cover/max_cover
    error=100*error/max_cover
    
    return(list(cover,error))
}