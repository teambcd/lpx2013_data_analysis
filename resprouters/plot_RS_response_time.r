configure_plot_repsonse_time <-function() {
    
    source("../libs/make_transparent.r")
    source("../libs/install_and_source_library.r")
    install_and_source_library("ncdf")
    install_and_source_library("ncdf4")
    install_and_source_library("stats")
    
    ##Observations
    obs_filename        <<- 'data/Recovery_data.csv'
    
    ##  Modelled data:
    LPX_varname         <<- 'fpc_grid'
    #LPXrs_filename      <<- '../../Model/git/lpd-resprout/Outputs/rs_fpc_test2_d66183-3136.nc'
    #LPXnr_filename      <<- '../../Model/git/lpd-resprout/Outputs/NR_fpc_test_d66183-3136.nc'
    #LPXrs_nf_filename   <<- '../../Model/git/lpd-resprout/Outputs/rs_nf_fpc_test_d66183-3136.nc' 
    #LPXnr_nf_filename   <<- '../../Model/git/lpd-resprout/Outputs/nf_fpc_test_d66183-3136.nc'
    LPXrs_filename      <<- 'data/LPX-RS/rs_fpc_test3_d66183-3136.nc'
    LPXnr_filename      <<- 'data/LPX-NR/NR_fpc_test_d66183-3136.nc'
    LPXrs_nf_filename   <<- 'data/LPX-RS/rs_nf_fpc_test_d66183-3136.nc' 
    LPXnr_nf_filename   <<- 'data/LPX-NR/nf_fpc_test_d66183-3136.nc'
    
	include_responses	<<- c(	LPX_RS=TRUE,LPX_NR=TRUE,
								Obs_RS=TRUE,Obs_OS=FALSE,
								Obs_UR=FALSE,Obs_NR=FALSE,Obs_all=TRUE)
   
    startyr             <<- 3139
    endyr               <<- 3156
    cal_yr0             <<- 1989
    countyr             <<- 25
    fire_yr             <<- 2
    
    xlabel_name         <<- 'year';              
    ylabel_name         <<- 'recovery index (%)';  
    alpha               <<- 0.5
    figure_name         <<- 'figure/RS_responseN.pdf'
    figure_height       <<- 5
    figure_width        <<- 6

}



plot_RS_response_time <- function() {
    configure_plot_repsonse_time()
    ##set up plot
    years=startyr:(startyr+countyr-1)
    cal_yr=years+cal_yr0-years[1]
    
    set_up_plot(cal_yr)
    
    ##Plot observations
    plot_observations(cal_yr)
    
    ## Plot LPX  outputs    
    cal_yr=cal_yr[years<=endyr]
    years=years[years<=endyr] 
    print("for NR simulation:")
    if (include_responses["LPX_NR"])
    	NR=plot_LPX_runs(LPXnr_filename,LPXnr_nf_filename,years,cal_yr,'black')
    	
    print("for RS simulation:")
    if (include_responses["LPX_RS"])
    	RS=plot_LPX_runs(LPXrs_filename,LPXrs_nf_filename,years,cal_yr,'red')   
    
    dev.off()
    
}

set_up_plot <- function(x) {
    pdf(figure_name, height =figure_height, width=figure_width)
    
    plot(x,rep(0,length(x)),xaxs='i',yaxs='i',
    xlim=c(x[1]-1, x[length(x)]+1),ylim=c(0, 115),
        xlab=xlabel_name,ylab=ylabel_name,
        type = "n", cex=.5) 
    
    fire_yr=cal_yr0+fire_yr
    ## plot on 'recovery zone' of 90%
    lines(c(x[1]-1,x,x[length(x)]+1),rep(100,length(x)+2))
    lines(c(x[1]-1,x,x[length(x)]+1),rep(90,length(x)+2),lty=2)
    lines(c(fire_yr,fire_yr)-1,c(1,200),lty=4,lwd=7,col='orange')
    
    
    add_legend()
    
}

add_legend <- function() {
  plot_colours=c('red','black')
  quant_colours <- make.transparent(c('red','blue','brown','black','black'),alpha)
  
  standard_legend(plot_colours,quant_colours,lwd=c(1,1,30,30,30,30,30))
  standard_legend(plot_colours,quant_colours,lwd=2,lty=c(1,1,2,2,2,2,2))
}

standard_legend <- function(plot_colours,quant_colours,lwd=1,lty=1) {
	
	txt=c('LPX - RS', 'LPX - NR','Obs - RS','Obs - OS','Obs - ?R','Obs - NR','Obs')
	txt[!include_responses]=''
	
	cols=c(plot_colours,quant_colours)
	cols[!include_responses]="transparent"
	
  legend(x="right",legend=txt,border=FALSE,col=cols,bty="n",xpd=TRUE,lwd=lwd,lty=lty)
}

plot_observations <- function(cal_yr) {
    
    obs=read.csv(obs_filename)
    
    
    sites=1:max(obs$Site)
    
    obs_years=seq(cal_yr[1]-1,cal_yr[length(cal_yr)],1/12)
    
    ## All observations
    if (include_responses["Obs_all"])
    	plot_observation_section(obs,sites,obs_years,IQRt=TRUE)
    
    
    ## Just RS 
    test=(obs$RS==1)+(obs$RS==2)+(obs$RS==6)
	if (include_responses["Obs_RS"])
    	plot_observation_section(obs[which(test>0),],sites,obs_years,
    							 'red',alpha=alpha,TRUE)
    		
    ## Just OS
    test=(obs$RS==5)
    if (include_responses["Obs_OS"])
    	plot_observation_section(obs[which(test>0),],sites,obs_years,
       							 'blue',alpha=alpha,TRUE)
    
    ## Other RS
    test=(obs$RS==3)+(obs$RS==4)
    if (include_responses["Obs_UR"])
    	plot_observation_section(obs[which(test>0),],sites,obs_years,
    							 'brown',0.7,TRUE)
    
    ## mix
    test=(obs$RS==7)
    #plot_observation_section(obs[which(test>0),],sites,obs_years,
    #                         'gray',0.7,TRUE)
    
    ## NR
    test=(obs$RS==9)
    if (include_responses["Obs_UR"])
    	plot_observation_section(obs[which(test>0),],sites,obs_years,
                             	 'black',0.7,TRUE)   
}


plot_observation_section <- function(obs,sites,obs_years,
                colour='black',alpha=0.8,IQRt=FALSE) {
    
    test5=split_and_index_obs(5,obs,obs_years)
    obs_index=sapply(sites,split_and_index_obs,obs,obs_years)
    
    obs_min=apply(obs_index,1,min,na.rm=TRUE)
    obs_max=apply(obs_index,1,max,na.rm=TRUE)
    obs_men=apply(obs_index,1,mean,na.rm=TRUE)
    
    if (IQRt) {
        obs_std=apply(obs_index,1,IQR,na.rm=TRUE)
        obs_std[is.na(obs_std)]=0
        
        obs_min=obs_men-obs_std
        obs_max=obs_men+obs_std
    }
    
    obs_min[is.infinite(obs_min)]=NA
    obs_max[is.infinite(obs_max)]=NA
    
    test=is.na(obs_min+obs_max)==FALSE
    obs_min=obs_min[test]
    obs_max=obs_max[test]
    obs_men=obs_men[test]
    obs_years=obs_years[test]
    
    
    x=c(obs_years,rev(obs_years))
    y=c(obs_min,rev(obs_max))
    
    polygon(x,y,col=make.transparent(colour,alpha),border=NA)
    lines(obs_years,obs_men,col=make.transparent(colour,alpha),lty=2,lwd=2)
    
}

split_and_index_obs <- function(i,obs,yr=NaN,return_min=FALSE) {

    if (length(yr)==1 && is.na(yr)) {
        yr=seq(0,100,1/12)
        cal_yr0=0
        fire_yr=1
    }
    
    obsi=obs[which(obs$Site==i),]
    
    if (length(obsi$Index)<2) {
        return(rep(NA,length(yr)))
    }
    
    Year=obsi$Year
    Index=obsi$Index
    min_Index=min(Index)
    
    if (min_Index>0.9) {
        return(rep(NA,length(yr)))
    }
        
    
    ##extend back
    if (Year[1]>0) {
        grad=lm(Index[1:2] ~ Year[1:2])$coefficients[2]
        
        Index0=-grad*Year[1]+Index[1]
        
        Year=c(0,Year)
        Index=c(Index0,Index)
    }
    if (Year[1]>-1) {
        Year=c(-1,Year)
        Index=c(1,Index)
    }
    
    min_Index=min_Index/mean(Index[Year<0])
    
    
    Index=RS_index(Index,Year)
    ## Allow to extend foreward by two years
    a=length(Index)
    a0=which(Index==min(Index))
    
    IndexM=Index[a0:a]
    YearM=Year[a0:a]
    cfit=summary(nls(IndexM~100-100/(1+p*YearM),start=list(p=1)))$coefficients[1]
    
    Year=c(Year,Year[a]+1:100)
    Indexp=100-100/(1+cfit*Year[a]+1:100)
    
    Index=c(Index,Indexp)
    
    
    Year=Year+cal_yr0+fire_yr-1
    
    Index=approx(Year,Index,yr)$y
    ##Extend foreard if recovered    
    if (return_min) {
      return(list(Index,min_Index))
    }
    return(Index)

}


plot_LPX_runs <-function(filename,filename_nf,years,cal_yr,colour=1,cswitch=2) {
    
    NF_cover=sapply(years,load_LPX_data,filename_nf)
    ##load LPX
    cover=sapply(years,load_LPX_data,filename)
    
    ## calculate fraction of control
    if (cswitch==1) {
        ncover=cover/NF_cover[1]
    } else {
        ncover=cover/NF_cover
    }
    
    ## calculate as fraction on range
    ncover=RS_index(ncover) 
    ncover=c(100,ncover)
    cal_yr=c(cal_yr[1],cal_yr)
    cal_yr[2]=cal_yr[2]+11/12

    ##plot
    lines(cal_yr,ncover,col=colour,lwd=2)
    return(ncover)
    
}

RS_index <- function(cover,year=NaN) {
    
    if (length(year)==1) {
        year=fire_yr-1
    } else {
        year=which(abs(year+1.5)==min(abs(year+1.5)))
    }
    
    cover=cover-min(cover,na.rm=TRUE)  
    cover=100*cover/mean(cover[year])
    
    if (max(cover>120)) {
        #cover=120*cover/(max(cover))
    }
    
    return(cover)
}

load_LPX_data <-function(year,filename) {
    print(paste("   opening year:",year))
    n=nchar(filename)
    substr(filename,n-6,n-3)= as.character(year)

    nc=open.ncdf(filename)
    fpc=get.var.ncdf(nc, nc$var[[LPX_varname]])
    
    cover=rowSums(fpc[,,c(1:7,10:13)],dim=2)
    grass=rowSums(fpc[,,c(8,9)],dim=2)*0.4
    
    if (year==3140) {
      grass=grass*0.4
    }
    cover=cover+grass
    cover=sum(cover,na.rm=TRUE)/sum(is.na(cover)==FALSE)

    return(cover)

}