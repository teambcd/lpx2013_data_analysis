function adata=open_and_plot_annual_simulated_fpc(filename,syrs,npfts,tree_pfts)

    
    a=720;b=360;c=npfts;
    data=zeros(a,b,c,'single');

    [~,fl]=size(filename);
    [~,yl]=size(num2str(syrs));
    yr=syrs-1;
    for i=1:2
        yr=yr+1;
        filename(fl-yl-2:fl-3)=num2str(yr)

        data=data+open_netcdf_variable(0,filename,...
            'fpc_grid',[0 0 0],[a b c])/2;
    end

    data=sum(data(:,:,tree_pfts),3);
    data(data>9E9)=nan;
    
    adata(1:360,:)=data(361:720,:);
    adata(361:720,:)=data(1:360,:);

end