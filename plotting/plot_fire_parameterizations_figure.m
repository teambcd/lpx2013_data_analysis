% %% compare fire datasets
close all
clear all
addpath libs/

%% comparison area
rlat=[-45.25 -10.75 0.5];
rlon=[110.75 159.75 0.5];

%% GFED 3 file info
filename_GFED3='../../benchmark-system/bench_data/fire_GFED.nc';
varname_GFED3='mfire_frac';
syr_GFED3=1996.5;
seasonal_GFED3=true;
global_GFED3=true;
flip_GFED3=false;

%% GFED 4 file info
filename_GFED4='../../benchmark-system/bench_data/Fire_GFEDv4_Burnt_fraction_0.5grid9.nc';
varname_GFED4='mfire_frac';
syr_GFED4=1995.5;
seasonal_GFED4=true;
global_GFED4=true;
flip_GFED4=false;

%% AVHRR file info
filename_AVHRR='../../benchmark-system/bench_data/ff_AVHRR_global.nc';
varname_AVHRR='layer';


%% Bradstocks
filename_RossB='../../benchmark-system/bench_data/Ross_group_data.nc';
varname_RossB='afire_frac';
syr_RossB=1970.5;
seasonal_RossB=false;
global_RossB=false;
flip_RossB=true;

%% Old simulation
filename_old='../../Outputs/LPX/Historic/r184_mod_bench_cont-5147.nc';
syrs_old=5147;
nyrs_old=9;


%% Lightning
filename_lightn='../../Outputs/LPD/lightn/benchmarking_fire_fapar_fpc_npp_height_b6f06d8_lightn_Aus-10142.nc';
syrs_lightn=10147;
nyrs_lightn=9;


%% Drying
filename_dry='../../Outputs/LPD/drying/benchmarking_fire_fpc_npp_par_height_lm_inc_94f75c6_lpd_drying_Aus-10142.nc';
syrs_dry=10147;
nyrs_dry=9;


%% Litter
filename_litter='../../Outputs/LPD/litter/benchmarking_fire_fapar_fpc_npp_height_lm_inc_082651f_lpd_litter_Aus-10150.nc';
syrs_litter=10147;
nyrs_litter=9;


%% Roots
filename_roots='../../Outputs/LPD/roots/benchmarking_runoff_mfire_75b5091_roots_Aus-10147.nc';
syrs_roots=10147;
nyrs_roots=9;


%% bark
filename_bark='../../Outputs/LPD/bark/benchmarking_runoff_mfire_0cda63e_bark_Aus-7147.nc';
syrs_bark=7147;
nyrs_bark=9;


%% New simulation
filename_new='../../Outputs/LPD/all/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_e634b31_all_NR_Aus-5150.nc';
syrs_new=5147;
nyrs_new=9;


%% New simulation + RS
filename_RS='../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10149.nc';
syrs_RS=10147;
nyrs_RS=9;
          
          

%% define plotting range for annual average
ryrs=[1997 2006];

[nperiod,~]=size(ryrs);

%% open and orientate datasets

GFED3=open_and_orientate_obs_fire(filename_GFED3,varname_GFED3,...
    global_GFED3,flip_GFED3,rlat,rlon);
GFED4=open_and_orientate_obs_fire(filename_GFED4,varname_GFED4,...
    global_GFED4,flip_GFED4,rlat,rlon);
RossB=open_and_orientate_obs_fire(filename_RossB,varname_RossB,...
    global_RossB,flip_RossB,rlat,rlon);
AVHRR=open_netcdf_variable(0,filename_AVHRR,varname_AVHRR,0,0);
old=open_and_plot_annual_simulated(filename_old,syrs_old,nyrs_old);
lightn=open_and_plot_annual_simulated(filename_lightn,syrs_lightn,nyrs_lightn);
dry=open_and_plot_annual_simulated(filename_dry,syrs_dry,nyrs_dry);
litter=open_and_plot_annual_simulated(filename_litter,syrs_litter,nyrs_litter);
roots=open_and_plot_annual_simulated(filename_roots,syrs_roots,nyrs_roots);
bark=open_and_plot_annual_simulated(filename_bark,syrs_bark,nyrs_bark);
new=open_and_plot_annual_simulated(filename_new,syrs_new,nyrs_new);
RS=open_and_plot_annual_simulated(filename_RS,syrs_RS,nyrs_RS);



AVHRR=remove_outside(AVHRR(:,360:-1:1),rlat,rlon);
old=remove_outside(old,rlat,rlon);
lightn=remove_outside(lightn,rlat,rlon);
dry=remove_outside(dry,rlat,rlon);
litter=remove_outside(litter,rlat,rlon);
roots=remove_outside(roots,rlat,rlon);
bark=remove_outside(bark,rlat,rlon);
new=remove_outside(new,rlat,rlon);
RS=remove_outside(RS,rlat,rlon);
[a b,~]=size(GFED3);
AVHRR(isnan(old(:,:,1)))=0;
pdata=zeros(a,b,2,7);

%% make annual average plot

pdata(:,:,1,1)=plot_annual_average_over_range(GFED3,...
    syr_GFED3,ryrs,seasonal_GFED3,rlat,rlon);
pdata(:,:,1,2)=plot_annual_average_over_range(GFED4,...
    syr_GFED4,ryrs,seasonal_GFED4,rlat,rlon);
pdata(:,:,1,3)=AVHRR;
pdata(:,:,1,4)=plot_annual_average_over_range(RossB,...
    syr_RossB,ryrs,seasonal_RossB,rlat,rlon);

pdata(:,:,1,6)=old;

pdata(:,:,2,1)=lightn;
pdata(:,:,2,2)=roots;
pdata(:,:,2,3)=litter;
pdata(:,:,2,4)=dry;
pdata(:,:,2,5)=0.5*bark+0.5*old;
pdata(:,:,2,6)=new;
pdata(:,:,2,7)=RS;


Ross_mask=(isinf(pdata(:,:,1,4))+isnan(pdata(:,:,1,2)))==1;
pdata(pdata<0)=0;
pdata(isnan(pdata))=0;


temp=pdata(:,:,1,4);
temp(Ross_mask)=NaN;
pdata(:,:,1,4)=temp;

pdata=pdata*100;
pdata(pdata<0.00001)=0;

    load mask2
    load aus_land_shape
    plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'red'},...
        'limits',[0 0.001 0.01 0.02 0.05 0.1 0.2]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',0.5,'axis',[110 160 -45 -10]) 

