% %% compare fire datasets
close all
clear all
addpath libs/
load mask2
load Aus_land_shape


%% comparison area
rlat=[-45.25 -10.75 0.5];
rlon=[110.75 159.75 0.5];

%% benchmark data
filename_VCF='../../benchmark-system/bench_data/veg_cont_fields_CRU.nc';
varname_VCF='Tree_cover';


%% Old simulation
filename_old='../../Outputs/LPX/Historic/r184_mod_bench_cont-5147.nc';
syrs_old=5142;
npfts_old=9;
tree_pfts_old=[1:7];



%% New simulation
filename_new='../../Outputs/LPD/all/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_e634b31_all_NR_Aus-5150.nc';
syrs_new=5142;
npfts_new=13;
tree_pfts_new=[1:7 10:13];
          


%% RS simulation
filename_RS='../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10149.nc';
syrs_RS=10142;
npfts_RS=13;
tree_pfts_RS=[1:7 10:13];

%% agri mask
filename_crop='../../Inputs/Historical/cropland_18512006.nc';
filename_pas='../../Inputs/Historical/pasture_18512006.nc';
syrs_agri=142;
          

%% open and orientate datasets

VCF=open_and_orientate_obs_fpc(filename_VCF,varname_VCF,false,false,rlat,rlon);

old=open_and_plot_annual_simulated_fpc(filename_old,syrs_old,npfts_old,tree_pfts_old);
new=open_and_plot_annual_simulated_fpc(filename_new,syrs_new,npfts_new,tree_pfts_new);
RS=open_and_plot_annual_simulated_fpc(filename_RS,syrs_RS,npfts_RS,tree_pfts_RS);

old=remove_outside(old,rlat,rlon);
new=remove_outside(new,rlat,rlon);
RS=remove_outside(RS,rlat,rlon);
[a b,~]=size(VCF);

pdata=zeros(a,b,1,4);

%% make annual average plot

pdata(:,:,1,1)=VCF;
pdata(:,:,1,2)=old*100;
pdata(:,:,1,3)=new*100;
pdata(:,:,1,4)=RS*100;



pdata(pdata<0)=0;
pdata(isnan(pdata))=0;

pdata(pdata<0.1)=0;


    load mask2
    load aus_land_shape
    plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'dark_green'},...
        'limits',[0.01 0.02 0.05 0.1 0.2 0.5]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[110 160 -45 -10]) 

%% mask for crop

crop=open_and_orientate_obs_fpc(filename_crop,'crop',false,true,rlat,rlon);
crop=sum(crop(:,:,syrs_agri:syrs_agri+1),3)/2;

crop(crop<0)=0;

pdata0=pdata;
for i=2:4
    pdata(:,:,1,i)=pdata(:,:,1,i).*(1-crop);
end

plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'dark_green'},...
        'limits',[0.01 0.02 0.05 0.1 0.2 0.5]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[110 160 -45 -10]) 
    
    %% mask for agri
pas=open_and_orientate_obs_fpc(filename_pas,'pas',false,true,rlat,rlon);
pas=sum(pas(:,:,syrs_agri:syrs_agri+1),3)/2;

pas(pas<0)=0;

crop=crop+pas;


pdata=pdata0;
for i=2:4
    pdata(:,:,1,i)=pdata(:,:,1,i).*(1-crop);
end

plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'dark_green'},...
        'limits',[0.01 0.02 0.05 0.1 0.2 0.5]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[110 160 -45 -10]) 
    
    
    %% difference
    pdata=RS*100-new*100;
    pdata(isnan(pdata))=0;
    plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'dark_blue','dark_green'},...
        'limits',[-0.1 -0.05 -0.02 -0.01 0.01 0.02 0.05 0.1]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[110 160 -45 -10]) 
    
