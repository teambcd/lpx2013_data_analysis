MAT <-function()
  {
    filename="../../Inputs/Historical/tas_18512006.nc"
    varname="tas"
    startp=c(1,1,1441)
    countp=c(720,360,12)
    nyrs=30
    
    
    filename_out="Mean_temp.nc" #CV
    varname_out='mat'
    date_descp=paste("date:", Sys.Date(), sep =" ")
    long_name="Meant Annual Temperature" #CV
    units="degrees C" #CV
    descp="Mean tas"
    data_source="CRU TS3.0 LPX climate inputs used in Prentice et al (2011), using years 1970-2000" #CV
    convert_descp="Converted to MAT by Doug Kelley"
    sript_name="MAT" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/douglask3/lpx2013_data_analysis for scripts and contact information"
    
    
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    
    nc=open.nc(filename)
    
    data0=var.get.nc(nc,varname,start=startp,count=countp)    

    datai=rowMeans(data0,dim=2)
 
    startp[3]=startp[3]+countp[3]
    
    for (yr in 2:nyrs) {
        print(yr)
        
        data0=var.get.nc(nc,varname,start=startp,count=countp)    
        data0=rowMeans(data0,dim=2)
        
        datai=datai+data0
        startp[3]=startp[3]+countp[3]
    }
    
    
    datai2=datai
    datai2[1:360,]=datai[361:720,]
    datai2[361:720,]=datai[1:360,]
    
    datai=(datai2/nyrs)-273.15
    
    datai[which(datai<=-99)]=NaN
    
    nc=create.nc(filename_out,prefill=TRUE)
    dim.def.nc(nc, "lat", 360)
    dim.def.nc(nc, "lon", 720)

    ## Create two variables, one as coordinate variable
    var.def.nc(nc, "lat", "NC_FLOAT", "lat")
    var.def.nc(nc, "lon", "NC_FLOAT", "lon")
    var.def.nc(nc, varname_out, "NC_FLOAT", c("lon","lat"))
    
    att.put.nc(nc, varname_out, "missing_value", "NC_FLOAT", -99999.9)
    
    var.put.nc(nc, "lat", seq(-89.75,89.75,.5))
    var.put.nc(nc, "lon", seq(-179.75,179.75,.5))
    
    var.put.nc(nc, varname_out, matrix(NaN,countp[1],countp[2]))
    var.put.nc(nc, varname_out, datai)
    
    att.put.nc(nc, varname_out, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname_out, "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out, "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out, "Date created", "NC_CHAR", date_descp)  
	
    
  }
    
    
    
    